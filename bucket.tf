# Bucket to store website
resource "google_storage_bucket" "website" {
  provider = google
#  name     = "p-57068-albert-website"
  name     = var.gcs_bucket
  location = "US"
  force_destroy = true
}

# Make new objects public
resource "google_storage_default_object_access_control" "website_read" {
  bucket = google_storage_bucket.website.name
  role   = "READER"
  entity = "allUsers"
}

#resource "null_resource" "upload_folder_content" {
# triggers = {
#   file_hashes = jsonencode({
#   for fn in fileset(var.folder_path, "**") :
#   fn => filesha256("${var.folder_path}/${fn}")
#   })
# }

#provisioner "local-exec" {
#   command = "gsutil cp -r ${var.folder_path}/* gs://${var.gcs_bucket}/"
# }
#}

resource "google_storage_bucket_object" "hello" {
  name   = "hello.html"
  source = "web_static/hello.html"
#  bucket = "website"
  bucket  =var.gcs_bucket
}
